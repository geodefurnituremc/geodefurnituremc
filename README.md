**Michal & Company Geode Furniture**

Designers and furniture makers all over the world are incorporating geodes, precious gemstones, energy stones, and organic materials into furniture and home décor items. Whether it is a geode table, an agate table, geo lamp or any geode accessory, it will be a unique and aesthetic addition to a home or office décor. At Michal & Company we specialize in functional organic home decor, agate furniture, geode tables, agate tables and geo art pieces that spectacularly accent key spaces creating opulence, & life in abundance.


*[https://michalandcompany.com/geode-furniture/](https://michalandcompany.com/geode-furniture/)*



